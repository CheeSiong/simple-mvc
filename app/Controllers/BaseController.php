<?php
namespace App\Controllers;

use Core\MVC;

class BaseController extends MVC\Controller
{
	function __construct()
	{
		parent::__construct();
		
		$this->lang->load_lang = 'en';
	}
}