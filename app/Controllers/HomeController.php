<?php
namespace App\Controllers;
/**
* 
*/
class HomeController extends BaseController 
{
	
	function __construct()
	{
		parent::__construct();		
	}

	function index()
	{
		$this->view->render('app');
	}
}


