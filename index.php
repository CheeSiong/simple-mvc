<?php
/***
 *                  _     _          _     ______                                           _    
 *         /\      | |   | |        | |   |  ____|                                         | |   
 *        /  \   __| | __| | ___  __| |   | |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
 *       / /\ \ / _` |/ _` |/ _ \/ _` |   |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 *      / ____ \ (_| | (_| |  __/ (_| |   | |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   < 
 *     /_/    \_\__,_|\__,_|\___|\__,_|   |_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\
 *                                                                                               
 *                                                                                               
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// session_start();
ob_start();
ini_set('session.cache_limiter','public');
session_cache_limiter(false);
header("Cache-Control: max-age=300, must-revalidate"); 
header('Cache-Control: post-check=0', false);
require_once ('vendor/autoload.php');
require_once ('config/global.php');
require_once ('router.php');



