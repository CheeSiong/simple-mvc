<?php
//TimeZone Config
date_default_timezone_set("Asia/Kuala_Lumpur");

//Live Config
if (BASE_SERVER == 'passionation.co' || BASE_SERVER == 'staging.passionation.co') {
	define('LIVE', 1); // 1 yes, 0 no
	define('ENABLE', 1); // 1 yes, 0 no
	define('DEVELOPMENT', FALSE);
}
else{
	define('LIVE', 0);
	define('ENABLE', 0);
	define('DEVELOPMENT', FALSE);
}
//Site Config
define('RECAPTCHA_SECRET_KEY', '6LcxiyUUAAAAAJIjZqY9BS-u3DWIHNWX1Vvnc7Zs');
define('RECAPTCHA_SITE_KEY', '6LcxiyUUAAAAAF_8kpB62pU7YLmcc1RoW3HVp898');
define('MAX_PASSWORD_EXPRIED_TIME', ' +30 minutes');
define('POST_LIMIT', 8);
define('KOL_LIMIT', 8);
define('TOPIC_LIMIT', 6);
define('ARTICLE_LIMIT', 8);
define('VERSION', 'default_v2.0'); // for infinites & dotdot js version
define('PAST_CAMPAIGN', 2123231);


//Meta Config
if (BASE_SERVER == 'passionation.co') {
	define('META_TITLE', 'Passionation | Where brands and influencers meet.');
}
else {
	define('META_TITLE', 'Passionation.dev');
}
define('META_DESCRIPTION', 'Passionation drives brand conversation & engagement with our  creative influencer marketing platform. We enable brands to reach target audience effectively.');
define('META_KEYWORD', '');
define('META_IMAGE', BASE_URL . 'assets/img/meta_img.png');


//Facebook & DMP & GA Config
if(BASE_SERVER == 'passionation.co') {
	define('FACEBOOK_APP_ID', '141927779713516');
	define('GA_ID', 'UA-54224447-15'); //live
	define('GA_ID_CLIENT', 'UA-54224447-15'); //live
	define('DMP_SCRIPT', '1'); //live
}
else {
	define('FACEBOOK_APP_ID', '141927779713516');
	define('GA_ID', 'UA-84656809-10'); //staging
	define('GA_ID_CLIENT', 'UA-84656809-10'); //staging
	define('DMP_SCRIPT', '0'); //staging
}



//Email Config
if(BASE_SERVER == 'passionation.co') {
 	define('ADMIN_EMAIL', 'kol@innity.com');
 }
 else {
    define('ADMIN_EMAIL', 'cheesiong.leong@innity.com');
 }



//Cache Config
define('CACHE_PATH', './cache/');
$cache_folder = [
	CACHE_PATH . 'access_token/',
	CACHE_PATH . 'brands/',
	CACHE_PATH . 'categories/',
	CACHE_PATH . 'comment/',
	CACHE_PATH . 'directs/',
	CACHE_PATH . 'feeds/',
	CACHE_PATH . 'posts/',
	CACHE_PATH . 'queueing/',
	CACHE_PATH . 'sources/'
];
define('CACHE_FOLDER', $cache_folder);
define('CACHE_EXPRIED_TIME', 60*60);


