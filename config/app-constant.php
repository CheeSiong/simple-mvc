<?php
define('ROUTE_PATH', str_replace('index.php', '', rtrim($_SERVER['PHP_SELF'], '/')));
define('BASE_URL', 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['SERVER_NAME'] . ROUTE_PATH);
define('REDIRECT_URL', BASE_URL . substr($_SERVER['QUERY_STRING'], 1));

define('DATETIME_FORMAT', 'Y-m-d H:i:s');
//actual server name
if (strpos($_SERVER['SERVER_NAME'], 'www') !== FALSE) {
	$server_name = explode('www.', $_SERVER['SERVER_NAME']);
	define('BASE_SERVER', $server_name[1]);
}
else {
	define('BASE_SERVER', $_SERVER['SERVER_NAME']);
}

$framework = ['validation', 'pagination'];
define('FRAMEWORK', $framework);



