<?php
namespace Core\MVC;

use \Core\CoreSystem;

class View extends CoreSystem
{
	function __construct()
	{
		parent::__construct();
	}

	public function render($viewScript, $params = [])
	{
        extract($params);
		require_once('app/Views/' . $viewScript . '.php');
	}
}
