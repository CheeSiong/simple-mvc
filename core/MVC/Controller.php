<?php
namespace Core\MVC;

class Controller extends \Core\CoreSystem
{
	protected $view;
	protected $model;

	function __construct()
	{
		parent::__construct();
		$this->view = new View();
		$this->model = new Model();
	}
}