<?php
namespace Core\General;

class Request
{
	protected $query_set = [];

	function __construct()
	{
		$this->set();
	}

	function set()
	{
		$request_url = $_SERVER['REQUEST_QUERY'];
		$request_segment = explode('&', $request_url);
		$query_set = [];

		foreach ($request_segment as $key => $row) {
			if (!empty($row)) {
				$set = explode('=', $row);
				$query_set[$set[0]] = $set[1];
			}
		}
		$this->query_set = $query_set;
	}

	function variable($set = '', $default_set = '')
	{
		$var = '';
        $var = !empty($_POST[$set]) ? $_POST[$set] : '';
		if (array_key_exists($set, $this->query_set)) {
			$var = $this->query_set[$set];
		}
		if (!is_array($var)) {
			$var = !empty((string) $var) ? $var : $default_set;
		}
		else {
			$var = !empty($var) ? $var : $default_set;
		}
		return $var;
	}

	function get($set = '', $default_set = '')
	{

	}

	function post($set = '', $default_set = '')
	{

	}
}