<?php
namespace Core;

class Router
{
	protected $error;
	protected $direct_call = FALSE;

	public $hidden_segment_route;
	public static $routes = [];

	function __construct()
	{
		$this->error = new Error;
	}

	public static function get($url = '', $action = '')
	{
		Router::$routes['GET/' . $url] =  [
									'controller' => $action,
									'method' => 'GET'
								];
	}

	public static function post($url = '', $action = '')
	{
		Router::$routes['POST/' . $url] =  [
									'controller' => $action,
									'method' => 'POST'
								];
	}

	public function direct($url)
	{
		$url = $_SERVER['REQUEST_METHOD'] . '/' . $this->clean_url($url);
		$url_segment = explode('/', $url);
		$first_url_segment = $url_segment[1];

		$temp_request_query = explode('?', $_SERVER['REQUEST_URI']);
	    $request_query = isset($temp_request_query[1]) ? $temp_request_query[1] . '&' : '';

		foreach (Router::$routes as $key => $row) {
			$routers_url_segment = explode('/', $key);
			$first_routers_url_segment = $routers_url_segment[1];

			//match with first url and total array url
			if ($first_url_segment == $first_routers_url_segment && count($url_segment) == count($routers_url_segment)) {
				$call = [];
				$can_call = TRUE;
				$_SERVER['REQUEST_QUERY'] = '';

				// match with url type
				foreach ($routers_url_segment as $key_2 => $row_2) {
					//check is normal path or match path
					if (strpos($row_2, '=[') !== FALSE) {
						$pattern_segment = explode('=', $row_2);
						$pattern = $pattern_segment[1];
						$request_query .= $pattern_segment[0] . '=' . $url_segment[$key_2] . '&';
						$match_result = $this->string_pattern($url_segment[$key_2], $pattern);
						$call[] =  ($match_result != TRUE) ? FALSE : TRUE;
					}
					else {
						$key_path = explode('/', $key);
						foreach ($key_path as $key_3 => $row_3) {
							$call[] = ($url_segment[$key_2] == $row_2) ? TRUE : FALSE; 
						}
					}
				}
				
				//check see can call 
				foreach ($call as $key_4 => $row_4) {
					if (empty($row_4)) {
						$can_call = FALSE;
					}
				}

				if ($can_call == TRUE) {
					//check route method
					if ($row['method'] != $_SERVER['REQUEST_METHOD']) {
						$this->error->show('request error (' . $row['method'] . ')');
					}
					$this->direct_call = TRUE;
					$_SERVER['REQUEST_QUERY'] =  substr($request_query, 0, -1);;
					
					$this->core = new CoreSystem;
					$this->core->call($row['controller']);
					break;
				}
			}
		}

		if($this->direct_call == FALSE) {
			$this->error->show('url not found (' . $url . ')');
	    }
	}

	private function string_pattern($piece = '', $pattern = '') {
		if ($pattern == '[A-Z]') {
			//allow 	
			if(preg_match('/^[A-z]+$/', $piece) == 1) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		elseif ($pattern == '[0-9]') {
			if(preg_match('/^[1-9][0-9]*$/', $piece) == 1) {
				
				return TRUE;
			}
			else {
				return FALSE;
			}

		}
		elseif ($pattern == '[A-Z-0-9]') {
			//allow alphanumeric
			if(preg_match('/^[a-zA-Z0-9]+$/', $piece) == 1) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		elseif ($pattern == '[A-Z-0-9-Unicode]') {
			//allow alphanumeric & unicode
			if(preg_match('/^[\pL\pN.]+/u', $piece) == 1) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			$this->error->show('pattern not match (' . $pattern .')');
		}
	}

	private function clean_url($url)
	{
		// remove the query string link
		$url = explode('?', $url);
		// remove extention
		$url = explode('.', $url[0]);
		return $url[0];
	}

	//make for self passionation purpose, not part of framework
	private function additional_url()
	{
		//actual url
		//hidden segment
	}

}