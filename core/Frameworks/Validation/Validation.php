<?php
//This function only for validation

namespace Core\Frameworks\Validation;

use \Core\Frameworks\FrameworkBase;

class Validation extends FrameworkBase
{	
	protected $class_called = '';
	protected $rules = '';
	protected $rules_set = [];
	protected $callback_rules = [];
	protected $variable = [];
	protected $special_variable = [];
	protected $valid_name = '';
	protected $old_variable = [];
	protected $error_msg = [];
	protected $valid = [TRUE];
	protected $return_data = [];
	protected $lang = [];

	function __construct()
	{
		parent::__construct();
		// $this->lang = new Language();
	}

	//general function
	function trim()
	{
		foreach ($this->variable as $key => $row) {
			trim($this->variable[$key]);
		}
	}

	function required()
	{
		foreach ($this->variable as $key => $row) {
			$row = (string) $row;
			if(empty($row)) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('field_is_required');
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function at_least()
	{
		$x = 0;
		$key_display = '';
		foreach ($this->variable as $key => $row) {
			if (is_array($row)) {
				$key_display = $key;
				foreach ($row as $key_2 => $row_2) {
					if (!empty($row_2)) {
						$x++;
					}
				}
			}
		}
		if($x < $this->special_variable) {
			$this->error_msg[$this->valid_name][] = $this->lang->tran('at_least') . ' ' . $this->special_variable . ' ' . $key_display . ' ' . $this->lang->tran('is_required');
			$this->set_errror_msg();
		}
	}

	function min()
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		foreach ($this->variable as $key => $row) {
			if($need_required == 1 && empty($row)) {
				return;
			}
			$row = strlen((string) $row);
			if ($row < $this->special_variable) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('must_contain_at_least') . ' ' . $this->special_variable . ' ' . $this->lang->tran('characters');
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function max()
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		foreach ($this->variable as $key => $row) {
			if($need_required == 1 && empty($row)) {
				return;
			}
			$row = strlen((string) $row);
			if ($row > $this->special_variable) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('must_contain_no_more_than') . ' ' . $this->special_variable . ' characters';
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function match()
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		foreach ($this->variable as $key => $row) {
			if($need_required == 1 && empty($row)) {
				return;
			}
			if($row != $this->special_variable) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('field_is_not_match');
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function valid_email()
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		foreach ($this->variable as $key => $row) {
			if($need_required == 1 && empty($row)) {
				return;
			}
			if(!filter_var($row, FILTER_VALIDATE_EMAIL)) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('field_is_not_a_valid_email');
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function alphanumeric()
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		foreach ($this->variable as $key => $row) {
			if($need_required == 1 && empty($row)) {
				return;
			}
			if(preg_match('/[\^£$%&*()}{@#~?><>,|=_+¬-]/', $row)) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('field_is_not_valid');
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function number()
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		foreach ($this->variable as $key => $row) {
			if($need_required == 1 && empty($row)) {
				return;
			}
			if(preg_match("/[a-z]/i", $row)) {
				$this->error_msg[$this->valid_name][] = $key . ' ' . $this->lang->tran('field_is_not_a_valid_number');
			}
		}
		if(!empty($this->error_msg)) {
	    	$this->set_errror_msg();
	    }
	}

	function url()
	{
		$regex = "((https?|ftp)\:\/\/)?"; // SCHEME 
	    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
	    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
	    $regex .= "(\:[0-9]{2,5})?"; // Port 
	    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
	    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
	    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 


	}


	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////


	function set_rules($variable = [], $rules = '', $valid_name = '')
	{   
	    $this->rules = $rules;
	    $this->variable = $variable;
	    $this->valid_name = $valid_name;
	    $this->set_old_variable();
	    $this->rules_set[$valid_name] = [
	    	'rules' => $rules,
	    	'variable' => $variable 
	    ];
	}

	function set_old_variable()
	{
		$this->old_variable[$this->valid_name] = $this->variable;
	}

	function set_errror_msg()
	{		
    	$this->valid[] = FALSE;
	}

	function get_error_msg($valid_name = '')
	{
		//unset error variable
		foreach ($this->old_variable as $key => $row) {
				foreach ($this->error_msg as $key_2 => $row_2) {
					if($key == $key_2) {
						unset($this->old_variable[$key]);
					}
				}
		}

		return $this->error_msg;
	}

	function get_old_variable($valid_name = '')
	{
		if(!empty($valid_name)) {
			return $this->old_variable[$valid_name];
		}
		else{
			return $this->old_variable;
		}
	}

	function old($valid_name = '', $variable = '')
	{
		$value = '';
		if (!empty($valid_name)) {
			$value = !empty($this->old_variable[$valid_name][$variable]) ? $this->old_variable[$valid_name][$variable] : '';
		}

		return $value;
		// $value = '';
		// return $this->old_variable[$name];
		// return $name;
	}

	function get_return_data($valid_name = '')
	{
		if(!empty($valid_name)) {
			return $this->return_data[$valid_name];
		}
		else{
			return $this->return_data;
		}
	}

	function run($class_called = '')
	{
		foreach ($this->rules_set as $key => $row) {
			foreach ($row as $key_2 => $row_2) {
				if($key_2 == 'rules') {
					$rules = explode('|', $row_2);
					foreach ($rules as $key_3 => $row_3) {
						if(strpos($row_3, '[') !== FALSE) { //special rules
							$special_rules = explode('[', $row_3);
							$this->variable = $this->rules_set[$key]['variable'];
							$this->special_variable =  $this->request->variable(str_replace(']', '', $special_rules[1]));
							$this->valid_name = $key;
							if(empty($this->special_variable)) {
								$this->special_variable = str_replace(']', '', $special_rules[1]);
							}
							if(!empty($this->special_variable)) {
								$this->valid_name;
								$special_rules = $special_rules[0];
								$this->$special_rules();
							}
						}
						elseif(strpos($row_3, 'callback') !== FALSE) { //callback rules
							$this->class_called = $class_called;
							$this->callback($key, $row_3);
						}
						else{ //general rules
							$this->variable = $this->rules_set[$key]['variable'];
							$this->valid_name = $key;
							$this->$row_3();
						}
					}
				}

			}
		}

		foreach ($this->valid as $key => $row) {
			if(empty($row)) {
				$valid = FALSE;
				break;
			}
			else{
				$valid = TRUE;
			}
		}
		return $valid;
	}

	function callback($key = '', $row)
	{
		$need_required = $this->required_priority($this->rules_set[$this->valid_name]['rules']);
		if($need_required == 1 && empty($this->rules_set[$key]['variable'])) {
			return;
		}
		
		$call = 'App\Controllers\\' . $this->class_called;
		$callback_class = new $call;
		$callback_class->variable =  $this->rules_set[$key]['variable'];
		$callback_class->valid_name = $this->valid_name = $key;
		$callback_result = $callback_class->$row();
		if(isset($callback_result[0])) {
			if($callback_result[0] == FALSE) {
				$this->error_msg[$this->valid_name] = $callback_result['error_msg'];
				$this->set_errror_msg();	
			}
			else{
				if(!empty($callback_result['return_data'])) {
					$this->return_data[$this->valid_name] = $callback_result;
				}
			}
		}
		else{ //multiple level error
			foreach ($callback_result as $key => $row) {
				if($callback_result[$key][0] == FALSE) {
					$this->error_msg[$key][] = $row['error_msg'][0];
					$this->set_errror_msg();
				}
			}
		}
	}

	function required_priority($rules = '')
	{	
		$required_priority = FALSE;
		$rules = explode('|', $rules);
		foreach ($rules as $key => $row) {
			if($row == 'required') {

				$required_priority = TRUE;
				break;
			}
		}
		return $required_priority;
	}
}


  