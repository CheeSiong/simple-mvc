<?php
use Core\Router;

Router::get('index', 'HomeController@index');

$routes = new Router();
$routes->hidden_segment_route = ['country=en'];

$path = explode('index.php/', rtrim($_SERVER['PHP_SELF'], '/') . rtrim($_SERVER['QUERY_STRING'], '/'));
$url = !empty($path[1]) ? $path[1] : 'index';
$routes->direct($url);